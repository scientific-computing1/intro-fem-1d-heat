[![pipeline status](https://gitlab.com/scientific-computing1/intro-fem-1d-heat/badges/master/pipeline.svg)](https://gitlab.com/scientific-computing1/intro-fem-1d-heat/-/commits/master)

This repository uses the python script from [John Burkardt](https://people.sc.fsu.edu/~jburkardt/py_src/fem1d_bvp_linear/fem1d_bvp_linear.py) to solve a simple heat problem.
Gitlab is used to run the tests and publish the resulting images.

# 1D Heat problem solved by FEM

## Strong form

Let us start from a general coverning equation:

```math
- \frac{\mathbf{a} \partial^2 u}{\partial x^2} + \mathbf{c} u = f(x) 
```

The unknown temperature is $` u `$ and the one-dimensional (1D) domain varible is $` x `$. Let us consider the unit domain $` \Omega = ]0, 1[ `$, so $`0<x<1`$.

The physics of the heat problem is obtained by imposing $`\mathbf{a}=1`$ and $`\mathbf{c}=0`$. Toghether with the boundary conditions below, this yields to the strong form:


```math
- \frac{ \partial^2 u}{\partial x^2}   = f(x) 
```
In the [index notation](https://en.wikipedia.org/wiki/Einstein_notation) this simply writes.
```math
u_{ii} = f(x)

```

In this context the forcing term $`f(x)`$ is a heat source withing the domain.

## Boundary conditions

At the extrama of the domain the following boundary conditions (BCs) apply:

```math
u(0) = u(1) = 0
```

These are called _homogenous_ BC because equal to 0.

The forcing term $`f(x)`$ is defined to match the following fictitious analytical solution:

```math
 u^\star (x) = x - \frac{\sinh(x)}{\sinh(1)}
```

> Using such fictitious analytical solution is common practise in numerical modelling, this is useful to study the convergence of a method. Since we know an exact solution, we  can reverse-engineer all BCs and source terms to match that solution. By doing so the residual  $`R= u^\star - u`$ become a measure of the error - we shall see however, that there are better metrics for the error. 

## Results

![1DSolution](https://scientific-computing1.gitlab.io/intro-fem-1d-heat/fem1d_bvp_linear_test00.png)

# Theory
All the theory is documented in [this file.](https://scientific-computing1.gitlab.io/intro-fem-1d-heat/intro_to_FEM.pdf)


# Code documentation

The function defining a similar problem is 
`fem1d_bvp_linear_test00`
this is where the _input file_ is defined, however the value of the constant `c` has to be changed to reflect the describtion above.
This also includes two post-processing functions:
 1. Print various error metrics: local (per node) and global (as a result of an integegral over the domain)
 2. Plot the solution

The function 
`fem1d_bvp_linear`
performs all the computation, namely the steps 5, 6, 7, 8 and 9 above.


One of the error functions is
`l2_error_linear`
this is an useful metric

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright

Copyright 2020 Mattia Montanari
